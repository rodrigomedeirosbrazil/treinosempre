// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Clipboard from 'v-clipboard'

// Bootstrap
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit, faTrash, faDumbbell, faRedo, faListOl, faCopy, faUpload, faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// bootstrap
Vue.use(BootstrapVue)
// fontawesome
library.add(faEdit, faTrash, faDumbbell, faRedo, faListOl, faCopy, faUpload, faEllipsisV)
Vue.component('font-awesome-icon', FontAwesomeIcon)
// clipboard
Vue.use(Clipboard)

Vue.config.productionTip = false

Vue.treinos_db = Vue.prototype.treinos_db = []

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
