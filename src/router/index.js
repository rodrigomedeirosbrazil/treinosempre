import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import Treinos from '@/components/Treinos'
import Treino from '@/components/Treino'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/treinos',
      name: 'Treinos',
      component: Treinos
    },
    {
      path: '/Treino/:id',
      name: 'Treino',
      component: Treino
    }
  ]
})
