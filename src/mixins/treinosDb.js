var moment = require('moment')

export default {
  filters: {
    date: function (date) {
      var d = moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY')
      if (d === 'Invalid date') return ''
      else return d
    }
  },
  methods: {
    getTreinos: function () {
      if ((this.treinos = JSON.parse(localStorage.getItem('treinos_db'))) === null) this.treinos = []
      return this.treinos
    },
    getTreino: function (treinoId) {
      if ((this.treinos = JSON.parse(localStorage.getItem('treinos_db'))) === null) this.treinos = []
      var i = this.treinos.findIndex(x => x.id === treinoId)
      if (i < 0) {
        return null
      }
      return this.treinos[i]
    },
    saveTreinos: function () {
      localStorage.setItem('treinos_db', JSON.stringify(this.treinos))
    },
    novoTreino: function (nome) {
      var id = this.generateUUID()
      this.treinos.push(
        {
          id: id,
          nome: nome,
          data_criacao: moment().format('YYYY-MM-DD'),
          ultimo_treino: null,
          exercicios: []
        })
      this.saveTreinos()
      return id
    },
    novoExercicio: function (treino, dados) {
      var id = this.generateUUID()
      if (!this.treino.hasOwnProperty('exercicios')) this.treino.exercicios = []
      this.treino.exercicios.push(
        {
          id: id,
          nome: dados.nome,
          carga: dados.carga,
          repeticao: dados.repeticao,
          series: dados.series,
          data_criacao: moment().format('YYYY-MM-DD'),
          ultimo_treino: null
        })
      this.saveTreinos()
      console.log(treino, dados)
      return id
    },
    deleteTreino: function (id) {
      // TODO: adicionar confirmação
      var obj = this.treinos.findIndex(x => x.id === id)
      this.treinos.splice(obj, 1)
      this.saveTreinos()
    },
    deleteExercicio: function (treino, id) {
      // TODO: adicionar confirmação
      var obj = treino.exercicios.findIndex(x => x.id === id)
      this.treino.exercicios.splice(obj, 1)
      this.saveTreinos()
    },
    importaTreinos: function (treinos) {
      try {
        var dados = JSON.parse(treinos)
        for (var i = 0; i < dados.length; i++) {
          var exercicios = []
          if (dados[i].hasOwnProperty('exercicios')) {
            for (var e = 0; e < dados[i].exercicios.length; e++) {
              exercicios.push(
                {
                  id: this.generateUUID(),
                  nome: dados[i].exercicios[e].nome,
                  carga: dados[i].exercicios[e].carga,
                  repeticao: dados[i].exercicios[e].repeticao,
                  series: dados[i].exercicios[e].series,
                  data_criacao: dados[i].exercicios[e].data_criacao,
                  ultimo_treino: dados[i].exercicios[e].ultimo_treino
                })
            }
          }
          this.treinos.push(
            {
              id: this.generateUUID(),
              nome: dados[i].nome,
              data_criacao: dados[i].data_criacao,
              ultimo_treino: dados[i].ultimo_treino,
              exercicios: exercicios
            })
        }
        this.saveTreinos()
        return true
      } catch (error) {
        console.log('try catch', error)
        return false
      }
    },
    generateUUID: function () {
      var d = new Date().getTime()
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        var r = (d + Math.random() * 16) % 16 | 0
        d = Math.floor(d / 16)
        return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16)
      })
      return uuid
    }
  }
}
